##################################################
##  A pgi makefile for ASP on rufus				##
##  Matt Alvarado, 02/13/2012		## 
##################################################
SHELL = /bin/sh
##
## === Define files and directories
##

PRNTDIR = ../OUTPUT/
INCDIR  = ../INCLUDE/
EXEDIR  = ../

SOURCEDIR = ../src/
CHEMDIR = ../src/Chemistry/
COAGDIR = ../src/Coagulation/
COMPDIR = ../src/ComputationalSubroutines/
CONDDIR = ../src/Condensation/
EULERDIR = ../src/EulerianEnvironment/
INFDIR = ../src/InfrastructuralCode/
PARTDIR = ../src/LagrangianParticles/
OUTPUTDIR = ../src/OutputRoutines/
UNIFACDIR = ../src/UNIFAC/
OBJDIR = ./asp_linux_pgi_dbl.obj/

FILES	= $(SOURCEDIR)ModelParameters.f90 \
	  $(SOURCEDIR)ReadInputDeck.f90  \
          $(SOURCEDIR)StepASP.f90 \
	  $(SOURCEDIR)Time.f90 \
	  $(SOURCEDIR)EulerianBoxDriver.f90

MODULES = $(CHEMDIR)Chemistry.f90 \
          $(COAGDIR)Coagulation.f90 \
          $(CONDDIR)Condensation.f90 \
	  $(EULERDIR)GridPointFields.f90 \
          $(INFDIR)InfrastructuralCode.f90 \
	  $(PARTDIR)LagrangianAerosols.f90 \
          $(OUTPUTDIR)OutputRoutines.f90 \
	  $(UNIFACDIR)unifac.f90 \
          $(UNIFACDIR)soa_unidriv.f90 \
          $(UNIFACDIR)soa_unidriva.f90 \
	  $(COMPDIR)CDFNor.f90 \
          $(COMPDIR)dgaus8.f \
	  $(COMPDIR)Gibbs-DuhemEvaluationFunction.f90 \
	  $(COMPDIR)indexx.f \
          $(COMPDIR)lsodes.f \
          $(COMPDIR)PPND16.f
	 
CHEMINC = $(CHEMDIR)AqPhaseChemistryInits.h  $(CHEMDIR)AqPhaseReactionIntegration.h  \
	  $(CHEMDIR)ChemicalPropertyRoutines.h  $(CHEMDIR)ChemistryDerivativeStructures.h \
	  $(CHEMDIR)ChemistryParametersInitRoutines.h  $(CHEMDIR)EvolveGasChemistry.h \
	   $(CHEMDIR)GasPhaseChemistryInits.h  $(CHEMDIR)OrgPhaseChemInits.h

COAGINC = $(COAGDIR)CoagulationKernels.h

CONDINC = $(CONDDIR)AqOrgCondensationIntegrator.h \
          $(CONDDIR)binsolu.h \
	  $(CONDDIR)CondensationInitialization.h  \
          $(CONDDIR)CondensationIntegrator.h \
	  $(CONDDIR)CondensationRelatedFunctions.h  \
	  $(CONDDIR)HydrophobicCondensationFunctions.h \
	  $(CONDDIR)OrgCondensationIntegrator.h

EULERINC = $(EULERDIR)GasPhaseChemistry.h \
           $(EULERDIR)Initialize.h  \
	   $(EULERDIR)InterpolateGrid.h \
           $(EULERDIR)Pressure.h  \
           $(EULERDIR)SetGrid.h \
	   $(EULERDIR)Temperature.h \
           $(EULERDIR)Water.h

INFINC = $(INFDIR)Counters.h  \
         $(INFDIR)InputOutputCommands.h \
	 $(INFDIR)StringCommands.h

PARTINC = $(PARTDIR)InitializationAndSamplingRoutines.h  \
          $(PARTDIR)ParticleAttributes.h \
	  $(PARTDIR)ParticleStructure.h  \
          $(PARTDIR)SCTailorMakeParticle.h \
	  $(PARTDIR)SortAerosol.h \
          $(PARTDIR)Thermodynamics.h

UNIFACINC = $(UNIFACDIR)unifacparam.h \
            $(UNIFACDIR)unifacparamMattAq.h

ALLINC = $(CHEMINC) $(COAGINC) $(CONDINC) $(EULERINC) $(INFINC) $(PARTINC) $(UNIFACINC)

FILESINC = $(ALLINC)

FILESDUMP = asp 
	
##
## === Compile, link and execute:
## 

# Compile command -- single processor

#FC = /usr/local/pgi/linux86-64/10.5/bin/pgf90
FC = /opt/intel/Compiler/11.1/080/bin/intel64/ifort

#FFLAGS =  -fast -r8 -Mcache_align -Wl,-zmuldefs
FFLAGS = -fpp -O -r8 -prec-div -prec_div -DIFORT
#F90FLAGS := $(FFLAGS) -Mfree
F90FLAGS := $(FFLAGS)
LDFLAGS := 


OBJECTS :=
OBJECTS := $(OBJECTS) \
           $(OBJDIR)CDFNor.o $(OBJDIR)indexx.o $(OBJDIR)lsodes.o \
           $(OBJDIR)PPND16.o \
	   $(OBJDIR)dgaus8.o $(OBJDIR)DMiLay.o
OBJECTS := $(OBJECTS) \
	   $(OBJDIR)ModelParameters.o \
	   $(OBJDIR)InfrastructuralCode.o \
	   $(OBJDIR)GridPointFields.o \
	   $(OBJDIR)Time.o \
	   $(OBJDIR)Chemistry.o \
	   $(OBJDIR)Gibbs-DuhemEvaluationFunction.o \
           $(OBJDIR)LagrangianAerosols.o \
	   $(OBJDIR)Condensation.o $(OBJDIR)soa_unidriv.o \
           $(OBJDIR)soa_unidriva.o $(OBJDIR)unifac.o \
	   $(OBJDIR)OutputRoutines.o \
	   $(OBJDIR)Coagulation.o \
	   $(OBJDIR)ReadInputDeck.o \
	   $(OBJDIR)StepASP.o $(OBJDIR)DevelopmentDriver.o \
           $(OBJDIR)EulerianBoxDriver.o
	
# === Modules: 0 grid define; 1 essential; d type define; s all
MODASP := $(OBJDIR)DMiLay.o $(OBJDIR)CDFNor.o $(OBJDIR)indexx.o $(OBJDIR)lsodes.o \
           $(OBJDIR)PPND16.o \
	   $(OBJDIR)dgaus8.o \
	   $(OBJDIR)ModelParameters.o \
	   $(OBJDIR)InfrastructuralCode.o \
	   $(OBJDIR)GridPointFields.o \
	   $(OBJDIR)Time.o \
	   $(OBJDIR)Chemistry.o \
	   $(OBJDIR)Gibbs-DuhemEvaluationFunction.o \
           $(OBJDIR)LagrangianAerosols.o \
	   $(OBJDIR)Condensation.o $(OBJDIR)soa_unidriv.o \
           $(OBJDIR)soa_unidriva.o $(OBJDIR)unifac.o \
	   $(OBJDIR)OutputRoutines.o \
	   $(OBJDIR)Coagulation.o \
	   $(OBJDIR)ReadInputDeck.o \
	   $(OBJDIR)StepASP.o $(OBJDIR)DevelopmentDriver.o \
           $(OBJDIR)EulerianBoxDriver.o

MODS := $(MODASP)

PROG = asp_linux_ifort_dbl

##
## === Dependency
##

ALL: $(EXEDIR)$(PROG)

$(EXEDIR)$(PROG): $(OBJECTS)
	$(FC) $(FFLAGS) -o $@ $(OBJECTS) $(MODS) $(LDFLAGS)

OBJECTS : $(MODS) $(FILESINC)

$(OBJDIR)CDFNor.o: $(COMPDIR)CDFNor.f90	
	  $(FC) -c $(F90FLAGS) $<
	  \mv CDFNor.o $(OBJDIR)
$(OBJDIR)indexx.o: $(COMPDIR)indexx.f
	  $(FC) -c $(FFLAGS) $<
	  \mv indexx.o $(OBJDIR)
$(OBJDIR)lsodes.o: $(COMPDIR)lsodes.f
	  $(FC) -c $(FFLAGS) $<
	  \mv lsodes.o $(OBJDIR)
$(OBJDIR)PPND16.o: $(COMPDIR)PPND16.f
	  $(FC) -c $(FFLAGS) $<
	  \mv PPND16.o $(OBJDIR)
$(OBJDIR)dgaus8.o: $(COMPDIR)dgaus8.f
	  $(FC) -c $(FFLAGS) $<
	  \mv dgaus8.o $(OBJDIR)
$(OBJDIR)DMiLay.o: $(SOURCEDIR)DMiLay.f
	  $(FC) -c $(FFLAGS) $<
	  \mv DMiLay.o $(OBJDIR)
$(OBJDIR)unifac.o: $(UNIFACDIR)unifac.f
	  $(FC) -c $(FFLAGS) $<
	  \mv unifac.o $(OBJDIR)
$(OBJDIR)soa_unidriv.o: $(UNIFACDIR)soa_unidriv.f \
	$(OBJDIR)unifac.o $(UNIFACDIR)unifacparam.h
	  $(FC) -c $(FFLAGS) $<
	  \mv soa_unidriv.o $(OBJDIR)
$(OBJDIR)soa_unidriva.o: $(UNIFACDIR)soa_unidriva.f \
	$(OBJDIR)unifac.o $(UNIFACDIR)unifacparamMattAq.h
	  $(FC) -c $(FFLAGS) $<
	  \mv soa_unidriva.o $(OBJDIR)
$(OBJDIR)ModelParameters.o: $(SOURCEDIR)ModelParameters.f90 
	  $(FC) -c $(F90FLAGS) $<
	  \mv ModelParameters.o $(OBJDIR)
$(OBJDIR)Gibbs-DuhemEvaluationFunction.o: \
	$(COMPDIR)Gibbs-DuhemEvaluationFunction.f90
	  $(FC) -c $(F90FLAGS) $<
	  \mv Gibbs-DuhemEvaluationFunction.o $(OBJDIR)
$(OBJDIR)InfrastructuralCode.o: $(INFDIR)InfrastructuralCode.f90 \
	$(INFINC)
	  $(FC) -c $(F90FLAGS) $<
	  \mv InfrastructuralCode.o $(OBJDIR)
$(OBJDIR)GridPointFields.o: $(EULERDIR)GridPointFields.f90 \
	$(OBJDIR)ModelParameters.o \
	$(OBJDIR)InfrastructuralCode.o $(EULERINC) $(INFINC)
	  $(FC) -c $(F90FLAGS) $<
	  \mv GridPointFields.o $(OBJDIR)       	
$(OBJDIR)Time.o: $(SOURCEDIR)Time.f90 $(OBJDIR)ModelParameters.o       	
	  $(FC) -c $(F90FLAGS) $<
	  \mv Time.o $(OBJDIR)   
$(OBJDIR)Chemistry.o: $(CHEMDIR)Chemistry.f90 \
       $(OBJDIR)Time.o 	$(OBJDIR)ModelParameters.o \
       $(OBJDIR)InfrastructuralCode.o \
       $(OBJDIR)GridPointFields.o $(CHEMINC) $(EULERINC) $(INFINC)
	  $(FC) -c $(F90FLAGS) $<
	  \mv Chemistry.o $(OBJDIR)   
$(OBJDIR)ReadInputDeck.o: $(SOURCEDIR)ReadInputDeck.f90 \
	$(OBJDIR)GridPointFields.o $(OBJDIR)Chemistry.o \
	$(OBJDIR)ModelParameters.o\
	$(OBJDIR)InfrastructuralCode.o $(CHEMINC) \
	$(EULERINC) $(INFINC)
	  $(FC) -c $(F90FLAGS) $<
	  \mv ReadInputDeck.o $(OBJDIR)   
$(OBJDIR)LagrangianAerosols.o: $(PARTDIR)LagrangianAerosols.f90 \
	$(OBJDIR)GridPointFields.o   $(OBJDIR)Chemistry.o   \
	$(OBJDIR)ModelParameters.o \
	$(OBJDIR)InfrastructuralCode.o   $(OBJDIR)Time.o \
	$(CHEMINC) $(EULERINC) $(INFINC) $(AEROINC) $(PARTINC)
	  $(FC) -c $(F90FLAGS) $< 
	  \mv LagrangianAerosols.o $(OBJDIR)  
$(OBJDIR)Condensation.o: $(CONDDIR)Condensation.f90  \
	$(OBJDIR)GridPointFields.o $(OBJDIR)Chemistry.o \
	$(OBJDIR)ModelParameters.o \
	$(OBJDIR)InfrastructuralCode.o \
	$(OBJDIR)LagrangianAerosols.o $(CHEMINC) \
	$(EULERINC) $(INFINC) $(AEROINC) $(CONDINC)
	  $(FC) -c $(F90FLAGS) $<
	  \mv Condensation.o $(OBJDIR)   	
$(OBJDIR)OutputRoutines.o: $(OUTPUTDIR)OutputRoutines.f90 \
	$(OBJDIR)GridPointFields.o   $(OBJDIR)Chemistry.o \
	$(OBJDIR)ModelParameters.o \
	$(OBJDIR)InfrastructuralCode.o   $(OBJDIR)LagrangianAerosols.o \
	$(OBJDIR)Condensation.o $(CHEMINC) \
	$(EULERINC) $(INFINC) $(AEROINC) $(CONDINC)
	  $(FC) -c $(F90FLAGS) $<  
	  \mv OutputRoutines.o $(OBJDIR) 
$(OBJDIR)Coagulation.o: $(COAGDIR)Coagulation.f90  \
	$(OBJDIR)GridPointFields.o   $(OBJDIR)Chemistry.o \
     	$(OBJDIR)ModelParameters.o \
	$(OBJDIR)InfrastructuralCode.o $(OBJDIR)LagrangianAerosols.o \
	$(OBJDIR)OutputRoutines.o $(CHEMINC) \
	$(EULERINC) $(INFINC) $(AEROINC) $(CONDINC) $(COAGINC) 
	  $(FC) -c $(F90FLAGS) $<  
	  \mv Coagulation.o $(OBJDIR) 
$(OBJDIR)StepASP.o: $(SOURCEDIR)StepASP.f90 \
	$(OBJDIR)GridPointFields.o   $(OBJDIR)Condensation.o \
       	$(OBJDIR)ModelParameters.o \
	$(OBJDIR)Time.o $(OBJDIR)LagrangianAerosols.o $(ALLINC)
	  $(FC) -c $(F90FLAGS) $<  
	  \mv StepASP.o $(OBJDIR) 
$(OBJDIR)EulerianBoxDriver.o: $(SOURCEDIR)EulerianBoxDriver.f90 \
	$(OBJDIR)GridPointFields.o   $(OBJDIR)Condensation.o \
       	$(OBJDIR)ModelParameters.o \
	$(OBJDIR)Time.o $(OBJDIR)LagrangianAerosols.o $(ALLINC)
	  $(FC) -c $(F90FLAGS) $<  
	  \mv EulerianBoxDriver.o $(OBJDIR) 
$(OBJDIR)DevelopmentDriver.o: $(SOURCEDIR)DevelopmentDriver.f90 \
	$(OBJECTS) $(ALLINC)
	  $(FC) -c $(F90FLAGS) $< 
	  \mv DevelopmentDriver.o $(OBJDIR)  
#	  \mv *.mod $(OBJDIR) 
#
# === Rules
#

.SUFFIXES: .mod .F .F90 .f90 .f

.F90.mod:
	$(FC) -c $(F90FLAGS) $<

.F.mod:
	$(FC) -c $(FFLAGS) $<

.F90.o:
	$(FC) -c $(F90FLAGS) $<

.F.o:
	$(FC) -c $(FFLAGS) $<

.f90.mod:
	$(FC) -c $(F90FLAGS) $<

.f.mod:
	$(FC) -c $(FFLAGS) $<

.f90.o:
	$(FC) -c $(F90FLAGS) $<

.f.o:
	$(FC) -c $(FFLAGS) $<

##
## === Functions
##

clean:
	rm -f  $(OBJDIR)*.o $(OBJDIR)*.mod $(OBJDIR)*.il $(OBJDIR)*.d $(OBJDIR)*.pc*

cleandump:
	rm $(FILESDUMP) $(OBJECTS)

tags:
	rm -rf ./HTML/ftagshtml
	/project/rs_sw/local/ftagshtml-0.522/bin/ftagshtml -latex -nojava -f90 -l2h $(FILES) $(MODULES) $(FILESINC) $(SOURCEDIR)DevelopmentDriver.f90

