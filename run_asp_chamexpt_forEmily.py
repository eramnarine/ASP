#-------------------------------------------------------------------
# This script runs ASP for look-up table runs (P1704) using ARCTAS data
# for a single flight.
#
# Created by: Emily Ramnarine, May 2017
# Based on perl script created by Matt Alvarado
#
# Right now the output is a little funny but it does the job
#-------------------------------------------------------------------

import os
import shutil as util

#-------------------------------------------------------------------
# Inputs
#-------------------------------------------------------------------
asp_exec = ['asp_linux_ifort_dbl'] #executable

# Paths
dir_asp_run = '/home/eram/AER/FLAME3/'

dir_asp_input_files = '{}/InorganicInputDecks/'.format(dir_asp_run)
dir_asp_output_files = '{}/OutputFiles/'.format(dir_asp_run)

# ASP
asp_path = [dir_asp_run]

#Specify a few input decks or from a batch like a directory
AeroWallLoss = 'Off'
for GasWallLoss in ['On', 'Off']:
    aspinputdeck_arr = ['ASPInputDecks_Aero{}/ASPInputDeck37.in'.format(
                                            AeroWallLoss),
                        'ASPInputDecks_Aero{}/ASPInputDeck38.in'.format(
                                            AeroWallLoss),
                        'ASPInputDecks_Aero{}/ASPInputDeck40.in'.format(
                                            AeroWallLoss),
                        'ASPInputDecks_Aero{}/ASPInputDeck42.in'.format(
                                            AeroWallLoss),
                        'ASPInputDecks_Aero{}/ASPInputDeck43.in'.format(
                                            AeroWallLoss),
                        'ASPInputDecks_Aero{}/ASPInputDeck45.in'.format(
                                            AeroWallLoss),
                        'ASPInputDecks_Aero{}/ASPInputDeck47.in'.format(
                                            AeroWallLoss),
                        'ASPInputDecks_Aero{}/ASPInputDeck49.in'.format(
                                            AeroWallLoss)
                        ]
    aspinputdeck_aer_arr = ['AerosolModes37.in',
                            'AerosolModes38.in',
                            'AerosolModes40.in',
                            'AerosolModes42.in',
                            'AerosolModes43.in',
                            'AerosolModes45.in',
                            'AerosolModes47.in',
                            'AerosolModes49.in',
                            ]
    aspinputdeck_gas_arr = ['GasPhaseChems37.in',
                            'GasPhaseChems38.in',
                            'GasPhaseChems40.in',
                            'GasPhaseChems42.in',
                            'GasPhaseChems43.in',
                            'GasPhaseChems45.in',
                            'GasPhaseChems47.in',
                            'GasPhaseChems49.in',
                            ]
    mechanismgas_arr = ['GasChemicalMechanism_Gas{}.in'.format(GasWallLoss)]

    n_asp_exec = len(asp_exec) # number of executables
    n_aspinputdeck = len(aspinputdeck_arr) #number of experiments

    #-------------------------------------------------------------------
    # Run ASP
    #-------------------------------------------------------------------
    for iasp in range(0, n_asp_exec): # loops through executables
        aspinputdeck_1_arr = []
        aspinputdeck_aer1_arr = []
        aspinputdeck_gas1_arr = []
        aspinputdeck_mech_arr = []

        for ir in range(0, n_aspinputdeck): # loops through experiments
            os.chdir(dir_asp_input_files)
     
            aspinputdeck_1_arr.append(aspinputdeck_arr[ir])
            aspinputdeck_aer1_arr.append(aspinputdeck_aer_arr[ir])
            aspinputdeck_gas1_arr.append(aspinputdeck_gas_arr[ir])
            aspinputdeck_mech_arr.append(mechanismgas_arr[0])
            run_id = aspinputdeck_arr[ir][-5:-3]
            print('run_id: {}'.format(run_id))
        
            os.unlink('ASPInputDeck.in') #remove old ASPInputDeck.in
            os.link(aspinputdeck_1_arr[ir], 'ASPInputDeck.in')
            print('ASPInputDeck.in: ', aspinputdeck_1_arr[ir])
               
            os.unlink('AerosolModes.in') #remove old AerosolModes.in
            os.link(aspinputdeck_aer1_arr[ir], 'AerosolModes.in')
            print('AerosolModes.in: ', aspinputdeck_aer1_arr[ir])

            os.unlink('GasPhaseChems.in') #remove old GasPhaseChem.in
            os.link(aspinputdeck_gas1_arr[ir], 'GasPhaseChems.in')
            print('GasPhaseChems.in: ', aspinputdeck_gas1_arr[ir])

            os.unlink('GasChemicalMechanism.in') #remove old GasChemicalMechanism.in
            os.link(aspinputdeck_mech_arr[0], 'GasChemicalMechanism.in')
            #os.link(aspinputdeck_mech_arr[ir], 'GasChemicalMechanism.in')
            print('GasChemicalMechanism.in: ', aspinputdeck_mech_arr[ir])

            #Run ASP
            os.chdir(dir_asp_run)
            asp_run = '{}{}'.format(asp_path[iasp], asp_exec[iasp])
            print('Running ASP: ', asp_run)
            os.system(asp_run)
        
            #output files to run's folder
            os.chdir(dir_asp_output_files)
            #util.rmtree('run_{}'.format(run_id))
            os.mkdir('run_{}_Gas{}'.format(run_id, GasWallLoss))
            print('********************')
            print(' Run: ', run_id, ' ')
            print('********************')

            for item in os.listdir(dir_asp_output_files):
                if not os.path.isdir(item):
                    util.move(item, '{}/run_{}_Gas{}/'.format(
                             dir_asp_output_files, run_id, GasWallLoss))
                 

        #rename files
#        os.chdir(dir_asp_output_files)
#        ext = '{}.txt'.format(run_id)
#        
#        print('********************')
#        print(' Run: ', ext, ' ')
#        print('********************')
#        
#        Trascript_exec = 'Transcript{}'.format(ext)
#        util.copy('Transcript.TXT', Trascript_exec)
#
#        Enhance_exec = 'EnhanceRatios{}'.format(ext)
#        util.copy('EnhanceRatios.txt', Enhance_exec)
#
#        Jacobian_exec = 'GasPhaseJacobian{}'.format(ext)
#        util.copy('GasPhaseJacobian.txt', Jacobian_exec)
#
#        ODE_exec = 'GasPhaseODESet{}'.format(ext)
#        util.copy('GasPhaseODESet.txt', ODE_exec)
#
#        for i in range(1, 11):
#            orig = 'Aerosol{}.txt'.format(i)
#            new = 'Aerosol{}_{}'.format(i, ext)
#            util.copy(orig, new)
#
#        for i in range(0, 130, 10) + [9]:
#            orig = 'SizeDist{}min.txt'.format(i)
#            new = 'SizeDist{}min_{}'.format(i, ext)
#            util.copy(orig, new)
#
#        for i in range(0, 130, 10):
#            orig = 'AeroOptCIS{}min.txt'.format(i)
#            new = 'AeroOptCIS{}min_{}'.format(i, ext)
#            util.copy(orig, new)
#
#        for i in range(1, 4):
#            orig = 'GasOut{}.txt'.format(i)
#            new = 'GasOut{}_{}'.format(i, ext)
#            util.copy(orig, new)
#
#        Aero_orig = 'AerosolOutChamber.txt'
#        Aero_new = 'AerosolOutChamber.{}'.format(ext)
#        util.copy(Aero_orig, Aero_new)
