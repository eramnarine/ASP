# NUM_DIST.PY
# This file reads in data from ASP OutputFiles and uses it to plot number and 
# size distributions as a function of time.
# Emily Ramnarine
# May 2017

import matplotlib as mpl
mpl.use("TkAgg")
import numpy as np
import matplotlib.pyplot as plt

# define the run and the time
run_types = ['both_on', 'aero_on', 'gas_on', 'both_off']
min_time, max_time = (0, 360)
time_step = 10

def load_data(run_type, run_id, time):
    '''

    '''
    fname = '/home/eram/AER/FLAME3/OutputFiles/{}/run_{}/SizeDist{}min.txt'.format(
        run_type, run_id, time)
    fid = open(fname)
    headers = fid.readline().split()
    
    edge1 = []
    edge2 = []
    radius = []
    number = []
    for line in fid:
        edge1.append(float(line.split()[4]))
        edge2.append(float(line.split()[5]))
        radius.append(float(line.split()[6]))
        number.append(float(line.split()[7]))
    # reformatting
    edges = list(edge1)
    edges.append(edge2[-1])
    edges = np.array(edges)
    middles = []
    for i in range(len(edges)-1):
        middle = ( edges[i] + edges[i+1] ) / 2.
        middles.append(middle)
    middles = np.array(middles)
    radius = np.array(radius)
    diameter = radius * 2.
    number = np.array(number)

    return number, edges, middles, diameter

def convert_data(edges, number):
    '''

    '''
    edges[0] = 10**( 2. * np.log10(edges[1]) - np.log10(edges[2]) )
    edges[-1] = 10**( 2. * np.log10(edges[-2]) - np.log10(edges[-3]) )

    logDp = np.log10(edges)
    dlogDp = logDp[1:] - logDp[:-1]
    dNdlogDp = number / dlogDp

    return logDp, dNdlogDp

def smooth_data(N):
    N[:,0] = 0.67 * N[:,0] + 0.33 * N[:,1]
    N[:,1:-1] = 0.25 * N[:,:-2] + 0.5 * N[:,1:-1] + 0.25 * N[:,2:]
    N[:,-1] = 0.33 * N[:,-2] + 0.67 * N[:,-1]
    return N

times = range(min_time, max_time+1, time_step)

for run_id in [37, 38, 40, 42, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 66]: #43, 67
    fig, ax = plt.subplots(nrows=2, ncols=2)
    fig.suptitle('Change in Number Distribution in Run {} Lights On'.format(run_id))
    for i in range(len(run_types)):
        run_type = run_types[i]
        data = []
        for time in times:
            number, edges, middles, diameter = load_data(run_type, run_id, time)
            logDp, dNdlogDp = convert_data(edges, number)
            data.append(dNdlogDp)
        data = np.array(data)
        data_smoothed = smooth_data(data)
        data_smoothed = np.ma.masked_less(data_smoothed, 0.02)

        #print np.log10(np.max(data_smoothed))

        EDGES, TIMES = np.meshgrid(edges, times)
        plot = ax[i%2, i/2].pcolormesh(TIMES, EDGES, np.log10(data_smoothed), 
                                       vmin=np.log10(0.02) , vmax=4.8 ,cmap='nipy_spectral')
        ax[i%2, i/2].annotate(s=run_type, xy=(250, 0.3E-1))
        ax[i%2, i/2].set_xlabel('Time (s)')
        ax[i%2, i/2].set_xticks(range(min_time, max_time+1, 60))
        ax[i%2, i/2].set_ylabel('Diameter ($\mu$m)')
        ax[i%2, i/2].set_yscale('log')
    plt.tight_layout()
    fig.subplots_adjust(top=0.9, bottom=0.3)
    cbar_ax = fig.add_axes([0.1, 0.15, 0.8, 0.02])
    cbar = plt.colorbar(plot, orientation='horizontal', label='dNdlogDp (cm$^{-3}$)', 
                        format=mpl.ticker.FormatStrFormatter('$10^{%2.1f}$'), cax=cbar_ax)
    plt.savefig('bananas_{}'.format(run_id))
plt.close('all')
