# MASS_DIST.PY

# Emily Ramnarine
# June 2017

import matplotlib as mpl
mpl.use("TkAgg")
import numpy as np
import matplotlib.pyplot as plt

# define the run and the time
run_types = ['both_on', 'aero_on', 'gas_on', 'both_off']
min_time, max_time = (0, 360)
time_step = 10
times = range(min_time, max_time+1, time_step)

def load_data(run_type, run_id, time):
    '''

    '''
    fname = '/home/eram/AER/FLAME3/OutputFiles/{}_dark/run_{}/SizeDist{}min.txt'.format(
        run_type, run_id, time)
    fid = open(fname)
    headers = fid.readline()
    
    edge1 = []
    edge2 = []
    radius = []
    mass = []
    for line in fid:
        edge1.append(float(line.split()[4]))
        edge2.append(float(line.split()[5]))
        radius.append(float(line.split()[6]))
        mass.append(float(line.split()[10]))
    # reformatting
    edges = list(edge1)
    edges.append(edge2[-1])
    edges = np.array(edges)
    middles = []
    for i in range(len(edges)-1):
        middle = ( edges[i] + edges[i+1] ) / 2.
        middles.append(middle)
    middles = np.array(middles)
    radius = np.array(radius)
    diameter = radius * 2.
    mass = np.array(mass)

    return mass, edges, middles, diameter

for run_id in [37, 38, 40, 42, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 66]: #43, 67
    plt.figure()
    plt.title('OA Mass Change in Run {} Lights Off'.format(run_id))
    for run_type in run_types:
        run_masses = []
        for time in times:
            mass, edges, middles, diameter = load_data(run_type, run_id, time) #[bins]
            run_masses.append(mass) #[time, bins]
        run_masses = np.array(run_masses)
        run_mass = run_masses.sum(axis=1)
        plt.plot(times, run_mass, label=run_type)
        plt.legend()
    plt.xlabel('Time (s)')
    plt.xticks(range(min_time, max_time+1, 30))
    plt.ylabel('total OA mass ($\mu$m cm$^{-3}$)')
    plt.ylim((0,460))
    plt.savefig('mass_plot{}_dark.png'.format(run_id))
plt.close('all')
