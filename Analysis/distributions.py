# DISTRIBUTIONS.PY
# This file reads in data from ASP OutputFiles and uses it to plot number and 
# size distributions as a function of time.
# Emily Ramnarine
# May 2017

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

# define the run and the time
run_id = 37
min_time, max_time = (0, 360)
time_step = 10

# setting up colormap
times = range(min_time, max_time+1, time_step)
num_colors = ( max_time - min_time ) / time_step
cm = plt.get_cmap('jet')
cNorm = plt.Normalize(vmin=min_time, vmax=max_time)
cs = mpl.cm.ScalarMappable(norm=cNorm, cmap=cm)
fig = plt.figure()
cs.set_array(times)
#Z = [[0,0],[0,0]]
#CS3 = plt.contourf(Z, times, cmap=cs)
#plt.clf()

for time in times:
    # load in the data
    fname = '/home/eram/AER/FLAME3/OutputFiles/run_{}/SizeDist{}min.txt'.format(run_id, time)
    fid = open(fname)
    headers = fid.readline().split()
    
    edge1 = []
    edge2 = []
    radius = []
    number = []
    for line in fid:
        edge1.append(float(line.split()[4]))
        edge2.append(float(line.split()[5]))
        radius.append(float(line.split()[6]))
        number.append(float(line.split()[7]))
    # reformatting
    edges = list(edge1)
    edges.append(edge2[-1])
    edges = np.array(edges)
    middles = []
    for i in range(len(edges)-1):
        middle = ( edges[i] + edges[i+1] ) / 2.
        middles.append(middle)
    middles = np.array(middles)
    radius = np.array(radius)
    diameter = radius * 2.
    number = np.array(number)

    # plot the number distribution
    i = ( time - min_time ) / time_step
    plt.plot(edge1, number, color=cm(1.*i/num_colors))
    print cm(1.*i/num_colors)

plt.colorbar(cs)
plt.show()
