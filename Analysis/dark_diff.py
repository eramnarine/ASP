# DARK_DIFF.PY

# Emily Ramnarine
# June 2017

import numpy as np
import matplotlib as mpl
mpl.use("TkAgg")
import matplotlib.pyplot as plt

run_types = ['both_on', 'aero_on', 'gas_on', 'both_off']
min_time, max_time = (0, 360)
time_step = 10
times = range(min_time, max_time+1, time_step)

def load_data(fname):
    '''

    '''
    fid = open(fname)
    headers = fid.readline()

    mass = []
    edge1 = []
    edge2 = []
    radius = []
    for line in fid:
        mass.append(float(line.split()[10]))
        edge1.append(float(line.split()[4]))
        edge2.append(float(line.split()[5]))
        radius.append(float(line.split()[6]))
    #reformatting
    edges = list(edge1)
    edges.append(edge2[-1])
    edges = np.array(edges)
    middles = []
    for i in range(len(edges)-1):
        middle = ( edges[i] + edges[i+1] ) / 2.
        middles.append(middle)
    middles = np.array(middles)
    radius = np.array(radius)
    diameter = radius * 2.
    mass = np.array(mass)
 
    return mass, edges, middles, diameter

def load_light(run_type, run_id, time):
    '''

    '''
    fname = '/home/eram/AER/FLAME3/OutputFiles/{}/run_{}/SizeDist{}min.txt'.format(
        run_type, run_id, time)
    light_mass, edges, middles, diameter = load_data(fname)
    return light_mass

def load_dark(run_type, run_id, time):
    '''

    '''
    fname = '/home/eram/AER/FLAME3/OutputFiles/{}_dark/run_{}/SizeDist{}min.txt'.format(
        run_type, run_id, time)
    dark_mass, edges, middles, diameter = load_data(fname)
    return dark_mass, edges, middles, diameter

def are_they_same(light, dark):
    '''

    '''
    diff = light - dark
    for i in range(len(diff)):
        if diff[i] > 1E-5:
            print np.sum(diff)
            return False
    return True
    
for run_id in [37, 38, 40, 42, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 66]: #43, 67
    for run_type in run_types:
        light_run_masses = []
        dark_run_masses = []
        for time in times:
            light_mass = load_light(run_type, run_id, time) #[bins]
            dark_mass, edges, middle, diameter = load_dark(run_type, run_id, time) #[bins]

            light_run_masses.append(light_mass) #[time, bins]
            dark_run_masses.append(dark_mass) #[time, bins]
        light_run_masses = np.array(light_run_masses)
        dark_run_masses = np.array(dark_run_masses)

        light_run_mass = light_run_masses.sum(axis=1)
        dark_run_mass = dark_run_masses.sum(axis=1)
        
        same = are_they_same(light_run_mass, dark_run_mass)
        if same:
            print run_id, run_type, 'lights make no difference'
        else:
            print run_id, run_type, 'runs change with light'
            plt.figure()
            plt.plot(times, light_run_mass, label='lights on')
            plt.plot(times, dark_run_mass, '--', label='lights off')
            #plt.fill_between(times, dark_run_mass, light_run_mass)
            plt.legend()
            plt.title(str(run_id) + run_type)
            plt.xlabel('Time (s)')
            plt.ylabel('total OA mass ($\mu$m cm$^{-3}$)')
            plt.show()
