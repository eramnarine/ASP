#!/bin/bash
#PBS -N ASP
### Number of nodes and processors per node.
### This job will run on 1 nodes with 1 cores per node
#PBS -l nodes=1:ppn=1
### Specify queue if necessary
##PBS -q batch
### Specified resources (walltime, memory, etc) if necessary
### Join standard output and standard error in one file
#PBS -j oe
#PBS -W group_list=pierce_group


echo started at `date`

. /usr/local/Modules/3.2.10/init/bash
module load netcdf/4.3.3.1-intel16.0.0

export PATH='/home/eram/anaconda2/bin:$PATH'

ulimit -s unlimited

cd /home/eram/AER/FLAME3
python run_asp_chamexpt_forEmily.py > log

echo ended at `date`

