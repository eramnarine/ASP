# CHANGE_PHOTORATES.PY
# Emily Ramnarine
# July 2017

#

fname = 'PhotoRates_old.in'
fid_in = open(fname, 'r')

fname = 'PhotoRates.in'
fid_out = open(fname, 'w')

line = fid_in.readline()
while True:
    fid_out.write(line)
    line = fid_in.readline()
    if not line.startswith('!'):
        break

fid_out.write(line[:21])
for i in range(51):
    fid_out.write('  0.000E+00')
fid_out.write('!\n')

data = fid_in.readlines()
for line in data:
    fid_out.write(line[:21])
    for i in range(51):
        fid_out.write('  0.000E+00')
    fid_out.write('!\n')

fid_in.close()
fid_out.close()
