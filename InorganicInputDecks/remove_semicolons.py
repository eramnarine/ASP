#!/usr/bin/env python

# Written by Chantelle Lonsdale, Mar 2017

#from numpy import *
import os

# read in all lines of original ASP input file
fname = raw_input('file: ')
inputfile = open(fname,'r')
lines = inputfile.readlines()
with open('NEW_{}'.format(fname), "wt") as fout:
	for lin in range(0,len(lines)):
		old_b = lines[lin].rstrip()
		b = lines[lin].rstrip()
		
		if b[len(b)-1] == ';':
			b = b[0:len(b)-1] # take away semicolons
			print(old_b)
			print(b)
		fout.write(b)
		fout.write('\n') # write to new file	
		#print(old_b)	
		#print(b)	
# link new file to old name
os.unlink(fname) #remove old
os.link('NEW_{}'.format(fname), fname)
os.unlink('NEW_{}'.format(fname)) #remove NEW
