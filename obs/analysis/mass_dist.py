# MASS_DIST.PY

# Emily Ramnarine
# July 2017

import matplotlib as mpl
mpl.use("TkAgg")
import numpy as np
import matplotlib.pyplot as plt
import time as dt

run_dates = ['09_19', '09_20', '09_21', '09_22', '09_23', '09_24', '09_25', '09_26', '09_28',
               '09_29', '09_30', '10_01', '10_02', '10_04', '10_05', '10_06', '10_07', '10_08']
run_ids = [37, 38, 40, 42, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 66, 67]

def find_start_end_times():
    '''

    '''
    f = open('../FLAME_chamber_parameters.csv', 'r')
    title = f.readline()
    header = f.readline()

    start_time = []
    end_time = []
    line = f.readline()
    while not line.startswith(',,,'):
        start_time.append(int(line.split(',')[4]) - 1)
        end_time.append(int(line.split(',')[5]) - 1)
        line = f.readline()

    return start_time, end_time

def load_datatimes(run_id, run_date, start_time, end_time):
    '''

    '''
    fname = '../run_{}/{}_09_smps_dN.txt'.format(run_id, run_date)
    fid = open(fname, 'r')

    while fname == fname:
        line = fid.readline()
        if line.startswith('Start Time'):
            timestamps = line.split()[2:]
            chosen_timestamps = timestamps[start_time:end_time+1]
            time = []
            for t in chosen_timestamps:
                time_object = dt.strptime(t, '%H:%M:%S')
                hours = time_object.tm_hour
                minutes = time_object.tm_min
                seconds = time_object.tm_sec
                time.append(hours * 3600 + minutes * 60 + seconds)
            time = np.array(time)
            time = time - time[0]
            fid.close()
            return time

def load_data(run_id, run_date):
    '''

    '''
    data = []
    fname = '../run_{}/{}_09_ams_org.txt'.format(run_id, run_date)
    fid = open(fname, 'r')
    contents = fid.readlines()
    for j in range(len(contents)):
        line = contents[j].split()
        data.append(float(line[1]))
    fid.close()

    return data

def load_model(run_id, time):
    '''

    '''
    fname = '/home/eram/AER/FLAME3/OutputFiles/both_on_dark/run_{}/SizeDist{}min.txt'.format(
                                                                run_id, time)
    fid = open(fname)
    fid.readline()

    mass = []
    for line in fid:
        mass.append(float(line.split()[10]))
    mass = np.array(mass)

    return mass

start_time, end_time = find_start_end_times()
indexer = range(len(run_dates))
indexer.remove(4) #run 43
indexer.remove(17) #run 67
for i in indexer:
    data_time = load_datatimes(run_ids[i], run_dates[i], start_time[i], end_time[i])
    data = load_data(run_ids[i], run_dates[i])

    model_time = range(0, 360, 10)
    run_masses = []
    for time in model_time:
        run_mass = load_model(run_ids[i], time)
        run_masses.append(run_mass)
    run_masses = np.array(run_masses)
    model_mass = run_masses.sum(axis=1)

    plt.figure()
    plt.title('OA Mass Change in Run {} Lights Off'.format(run_ids[i]))
    plt.plot(data_time, data, label='obs')
    plt.plot(np.array(model_time) * 60., model_mass, label='model')
    plt.xlabel('Time (s)')
    plt.legend()
    plt.ylabel('total OA mass ($\mu$m cm$^{-3}$)')
    plt.savefig('mass_dist{}.png'.format(run_ids[i]))
plt.close('all')
