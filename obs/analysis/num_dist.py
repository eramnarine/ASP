# NUM_DIST.PY

# Emily Ramnarine
# June 2017

import matplotlib as mpl
mpl.use("TkAgg")
import numpy as np
import matplotlib.pyplot as plt
import time as dt
from matplotlib.colors import LogNorm

run_dates = ['09_20', '09_21', '09_22', '09_23', '09_24', '09_25', '09_26', '09_28',
               '09_29', '09_30', '10_01', '10_02', '10_04', '10_05', '10_06', '10_07']
             # '09_19', '10_08'
run_ids = [38, 40, 42, 43, 45, 47, 49, 51, 53, 55, 57, 59, 61, 63, 65, 66]
             # 37, 67
def find_start_end_times():
    '''

    '''
    f = open('../FLAME_chamber_parameters.csv', 'r')
    title = f.readline()
    header = f.readline()

    start_time = []
    end_time = []
    line = f.readline()
    while not line.startswith(',,,'):
        start_time.append(int(line.split(',')[4]) - 1)
        end_time.append(int(line.split(',')[5]) - 1)
        line = f.readline()
    
    return start_time, end_time

def load_times(fname, start_time, end_time):
    '''

    '''
    fid = open(fname, 'r')

    while fname == fname:
        line = fid.readline()
        if line.startswith('Start Time'):
            timestamps = line.split()[2:]
            chosen_timestamps = timestamps[start_time:end_time+1]
            time = []
            for t in chosen_timestamps:
                time_object = dt.strptime(t, '%H:%M:%S')
                hours = time_object.tm_hour
                minutes = time_object.tm_min
                seconds = time_object.tm_sec
                time.append(hours * 3600 + minutes * 60 + seconds)
            time = np.array(time)
            time = time - time[0]
            fid.close()
            return time

def load_data(fname, start_time, end_time):
    '''

    '''
    fid = open(fname, 'r')
    diameter = []
    data = []

    lines = fid.readlines()
    for line in lines:
        d = line.split()[0]
        diameter.append(d)

        t_data = line.split()[1:]
        chosen_t_data = t_data[start_time:end_time+1]
        chosen_t_data = np.array(chosen_t_data).astype(np.float)
        data.append(chosen_t_data)
    diameter = np.array(diameter).astype(np.float)
    data = np.array(data)

    fid.close()
    return diameter, data

def convert_data(middles, number):
    '''

    '''
    edges = np.zeros(len(middles) + 1)
    for i in range(1, len(edges) - 1):
        edges[i] = np.mean([middles[i-1], middles[i]])
    edges[0] = middles[0]**2 / edges[1] #geometric mean
    edges[-1] = middles[-1]**2 / edges[-2]
    #edges[0] = middles[0] - (edges[1] - middles[0]) #arithmetic mean
    #edges[-1] = middles[-1] + (middles[-1] - edges[-2])

    logDp = np.log10(edges)
    dlogDp = logDp[1:] - logDp[:-1]
    dNdlogDp = number / dlogDp[:,np.newaxis]

    return edges, dNdlogDp

start_time, end_time = find_start_end_times()

for i in range(len(run_dates)):
    time_file = '../run_{}/{}_09_smps_dN.txt'.format(run_ids[i], run_dates[i])
    time = load_times(time_file, start_time[i], end_time[i])

    data_file = '../smps_trimv3/{}_09_smps_dN_trimv3.txt'.format(run_dates[i])
    diameter, data = load_data(data_file, start_time[i], end_time[i])
    data = np.ma.masked_less(data, 0.02)

    edges, dNdlogDp = convert_data(diameter, data)
    EDGES, TIME = np.meshgrid(edges, time)

    plt.figure()
    plt.pcolormesh(TIME, EDGES / 1000., np.log10(data.T), cmap='nipy_spectral', #)
                                #dNdlogDp.T, cmap='nipy_spectral')
                                vmin=np.log10(0.02) , vmax=4.8)
    plt.colorbar(orientation='horizontal', label='dNdlogDp (cm$^{-3}$)',# norm=LogNorm,
                 format=mpl.ticker.FormatStrFormatter('$10^{%2.1f}$'))
    plt.title('Change in Number Distribution in Run {} Lights Off'.format(run_ids[i]))
    plt.xlabel('Time (s)')
    plt.ylabel('Diameter ($\mu$m)')
    plt.yscale('log')
    plt.ylim((plt.ylim()[0], EDGES.max()/1000.))
    plt.tight_layout()
    plt.savefig('dark_banana_run{}.png'.format(run_ids[i]))
plt.close('all')
