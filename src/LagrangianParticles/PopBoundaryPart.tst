!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Add each of the individual modes to the BoundaryParticles() array     !!
!! NOTE: Creates a single, internally mixed size distribution            !!
!! as the sum of the input modes.				         !!   
!!                                                                       !!
!!This subroutine is like PopulateParticlesSectionsRightAway,            !!
!! but only for environmental                                            !!
!!(boundary) aerosol modes listed in EnvAerosolModes.in                 !!
!!Any changes made to PopulateParticlesSectionsRightAway                 !!
!! should be made here as well                                           !!
!!Matt Alvarado, 09/07/06                                                !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE PopBoundaryPart ()

	USE InfrastructuralCode, ONLY : INT2STR, REAL2STR, Transcript, &
                                        SetParticleID, ERROR

	USE ModelParameters,     ONLY : lengthscale,			&
					DomainX, DomainY, DomainZ,	&
					LagrSectSizeCutoff,		&
					ThermoBulkMode,			&
					HowManyBins,			&
					BinEdges,			&
					micron,				&
					SmallestAerosolPossible,        &
					Monodisperse,                   &
                                        Pi, AverageAerosolDensity

	USE GridPointFields,     ONLY : GetTemp, GetRelativeHumidity

	USE Chemistry,		 ONLY :	HowManyAqChems,			&
					AqMolecularMass,		&
					howmanyaqanions,                &
                                        howmanyaqcations,               &
					HowManyOrgChems,		&
					OrgMolecularMass,               &
                                        AqPhaseChemicalNames,           &
                                        HowManyAqOrgChems

	IMPLICIT NONE

	!! Local Variables
	INTEGER :: I, J, K, X, Y, Z, Q,	&
	           NumbParticles,	&
	           SectionalParticles,	&
		   LagrangianParticles

	REAL*8 :: II, Temperature, ln_sigma_sq, vtot, mtot, d_mass_mean
	REAL*8 :: del_diam, vrat, diam, mass_bin, store_num, d_num_mean
	REAL*8 :: sum_num, new_num, sum_mass, num_all_modes, rescale
	REAL*8 :: RescalingFactors(HowManyAerosolModes,HowManyAqChems+HowManyOrgChems)
        REAL*8 :: RH, InorgWaterContent

	CHARACTER (len= 1024) :: str
	LOGICAL :: Scaffolding, Transcribe, Jacobson
	TYPE(Particle), POINTER :: NewSection
	TYPE(Particle), POINTER :: Current, Trailer, SectionRunner

	Scaffolding= .TRUE.
	Transcribe = .TRUE.
        Jacobson = .TRUE. !Use Jacobson init or MELAM init

	IF(Scaffolding .OR. TRANSCRIBE) CALL Transcript("")
	IF(Scaffolding)	CALL Transcript(">>Entering PopBoundaryPart()<<")
	IF(Transcribe)	CALL Transcript("_Assigning_Particles_to_Particle_Linked_List_Structure_")

	
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!! Each of the modes has its own sectional representation !!
	!! for particles below the certain size cutoff.			  !!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	IF (HowManyBins .GT. 0. .OR. ThermoBulkMode) THEN
	
		IF (.NOT.ThermoBulkMode .AND. .NOT.Monodisperse) THEN
				DO J = 1, HowManyBins
					NewSection => MakeSection(I,J)

					!! Budge the new mode in at the head of the list
					IF (ASSOCIATED(BoundaryParticles%First)) THEN
						NewSection%Next		   => BoundaryParticles%First
						BoundaryParticles%First => NewSection
					ELSE
						BoundaryParticles%First => NewSection
						NULLIFY(NewSection%Next)
						!NewSection%Next		   => Null()
					END IF
				END DO

		ELSE IF (Monodisperse) THEN
			!Monodisperse Distribution
			!WRITE(*,*) "Check 0"
			HowManyBins = 1
			NewSection => MakeBoundaryMonodisperse()
			!WRITE(*,*) "Check 0.5"
			BoundaryParticles%First => NewSection
		
		ELSE
			!Bulk Mode
			NewSection => MakeBoundaryBulk()
			BoundaryParticles%First => NewSection

		END IF

	END IF

	IF(Scaffolding) WRITE(*,*) "Check 1", ThermoBulkMode 
	IF (.NOT.ThermoBulkMode .AND. .NOT.Monodisperse) THEN

!! Allocate all of the aerosol and place them in the appropriate structures:
			DO I = 1, MAX(1,HowManyEnvAerosolModes)
			!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			!! Scale the particle concentration by the number !!
			!! concentration and the domain volume.		  !!
			!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				!!WRITE(*,*) "Hi!"
				NumbParticles = ANINT(EnvAerosolModes(I,1) * DomainX/LengthScale * DomainY/LengthScale * DomainZ/LengthScale)
				!This is to make sure small modes are included
				!IF(NumbParticles .LT. 10) NumbParticles = 100
				SectionalParticles  = 0
				LagrangianParticles = 0
				IF(EnvAerosolModes(I,3) .NE. 0.0) THEN !Log-normal mode
					DO J = 1, NumbParticles
						!Matt changed this to hopefully put particles in sections right away
						CALL MakeBoundaryParticle(I, K, .FALSE., StructuredDistribution = (DFLOAT(J)-0.5)/NumbParticles)
						SectionalParticles  = SectionalParticles + 1 - K
						LagrangianParticles = LagrangianParticles + K
					END DO
					!WRITE(*,*) "Hi again!"

					!! Report the results and allocations of the population
					IF (TRANSCRIBE) CALL TRANSCRIPT ("")
					IF (TRANSCRIBE) CALL TRANSCRIPT ("For the '"//TRIM(EnvAerosolModeNames(I))//"' aerosol mode")
					IF (TRANSCRIBE) CALL TRANSCRIPT ("Allocated "//TRIM(INT2STR(LagrangianParticles))//" Lagrangian particles")
					IF (TRANSCRIBE) CALL TRANSCRIPT ("and "//TRIM(INT2STR(SectionalParticles))//" Sectional particles")
				
				ELSE !Exponential Mode
					CALL MakeBoundaryParticleExponential(I)
				END IF
			END DO
	

	END IF

	IF(Scaffolding) WRITE(*,*) "Check 1.5"

	!! If the user specified that the contain a certain mass of each 
	!! species then rescale everything
	IF (InputFlagRatioOrMass .EQ. 1) THEN
		
		!!This should only be called for monodisperse or bulk aerosol
		IF(NOT(Monodisperse) .AND. NOT(ThermoBulkMode)) &
			CALL ERROR ("You selected a sectional aerosol.  In this mode, you must specify the abundance of chemicals by "// &
						"the relative mass percentage using the second flag in EnvAerosolModes.in (0); you have selected another "// &
						"option.  Please remedy.")	

		DO I = 1, HowManyEnvAerosolModes
		DO J = 1, HowManyAqChems+HowManyOrgChems
			RescalingFactors(I,J) = 0.
		END DO ; END DO



			Current => BoundaryParticles%First

		10	IF (ASSOCIATED(Current)) THEN

				DO I = 2,HowManyAqChems
					RescalingFactors(Current%ParticleDistribution,I) = RescalingFactors(Current%ParticleDistribution,I) + &
																	   Current%AqChems(I)*Current%NumberOfParticles
				END DO

				DO I = 1,HowManyOrgChems
					RescalingFactors(Current%ParticleDistribution,HowManyAqChems+I) = RescalingFactors(Current%ParticleDistribution,HowManyAqChems+I) + &
																	   Current%OrgChems(I)*Current%NumberOfParticles
				END DO

				IF (ASSOCIATED(Current%Next)) THEN
					Current => Current%Next
					GOTO 10
				END IF
			END IF

		IF(Scaffolding) WRITE(*,*) "Check 2"


		!! Then Calculate the Scaling
		DO I = 1, MAX(1,HowManyEnvAerosolModes-1)
			DO J = 2, HowManyAqChems
				IF (RescalingFactors(I,J) .GT. 0.) &
					RescalingFactors(I,J) = EnvAerosolModes(I,J+3) /		&
											RescalingFactors(I,J) / 1.e12 / AqMolecularMass(J)
			END DO 
	
			DO J = 1, HowManyOrgChems
				IF (RescalingFactors(I,HowManyAqChems+J) .GT. 0.) &
					RescalingFactors(I,HowManyAqChems+J) = EnvAerosolModes(I,HowManyAqChems+J+3) /		&
											RescalingFactors(I,HowManyAqChems+J) / 1.e12 / OrgMolecularMass(J)
			END DO 
		
		
		END DO

		IF(Scaffolding) WRITE(*,*) "Check 3"
		
			Current => BoundaryParticles%First

		20	IF (ASSOCIATED(Current)) THEN

				DO I = 2,HowManyAqChems
					Current%AqChems(I) = Current%AqChems(I) * RescalingFactors(Current%ParticleDistribution,I)
				END DO

				DO I = 1,HowManyOrgChems
					Current%OrgChems(I) = Current%OrgChems(I) * RescalingFactors(Current%ParticleDistribution,HowManyAqChems+I)
				END DO
				
				IF (ASSOCIATED(Current%Next)) THEN
					Current => Current%Next
					GOTO 20
				END IF
			END IF
		 
	END IF


	IF(Scaffolding) WRITE(*,*) "Check 4"

	!! Now that all of the consitutents have been added,
	!! walk through the lists and equilibrate everything

	Current => BoundaryParticles%First
	NULLIFY(Trailer)
	!Trailer => Null()

90	IF (ASSOCIATED(Current)) THEN

		IF (Current%NumberOfParticles .GT. 0.0) THEN
			II = 0.
			DO I = 2, HowManyAqChems
				!IF (Current%AqChems(I) .NE. 0.0) WRITE(*,*) AqPhaseChemicalNames(I)
				II = II + Current%AqChems(I)
			END DO
			DO I = 2, HowManyOrgChems
				!IF (Current%AqChems(I) .NE. 0.0) WRITE(*,*) AqPhaseChemicalNames(I)
				II = II + Current%OrgChems(I)
			END DO

                        WRITE(*,*) "H2O: ", Current%AqChems(1)
			!! Add a token amount of water to avoid initial numerical problems
			IF (II .GT. 0.0) THEN
                           Current%AqChems(1) = II*1.
                        ELSE
                           !If only BC specified, particle is dry
                           Current%Dry = .TRUE.
                        ENDIF

			!WRITE(*,*) Current%NumberOfParticles
			IF (NOT(Current%Dry)) THEN 
			   CALL KusikMeissner (Current)
			   IF(Scaffolding) WRITE(*,*) "Before Equilibrate"
			   
                           CALL FindElectrolyteEquilibrium(Current, .TRUE., FirstEquilibration=.TRUE., ReturnType=Q)
			   IF(Scaffolding) WRITE(*,*) "After Equilibrate"
			   CALL EquilibriumWaterContentAmount (Current, RH, Q, .TRUE., InorgWaterContent)
                           Current%AqChems(1) = InorgWaterContent
			   IF(Scaffolding) WRITE(*,*) "After Water EQ"
                        ENDIF
			   !Not needed for boundary particles
                           !CALL ShellRefIndAndRad(Current)
		           !IF(Scaffolding) WRITE(*,*) "After ShellRefIndAndRad"
		
		END IF
		
		CALL RecalculateRadius (Current)
		IF(Scaffolding) WRITE(*,*) "After RecalculateRadius"
		IF(Scaffolding) WRITE(*,*) Current%EffectiveRadius
	

		Trailer => Current
		Current => Current%Next

		GOTO 90
	END IF

	!! Report what ended up with
	IF(NOT(Monodisperse) .AND. NOT(ThermoBulkMode)) THEN
           CALL TRANSCRIPT("")
	   CALL TRANSCRIPT("Reallocated "//TRIM(INT2STR(SectionalParticles))//" particles to sections and left "//		&
	       TRIM(INT2STR(LagrangianParticles))//" as lagrangian particles.")
	   CALL TRANSCRIPT("")
        ELSEIF(Monodisperse) THEN
           CALL TRANSCRIPT("")
	   CALL TRANSCRIPT("Monodisperse distribution.") 
	   CALL TRANSCRIPT("")
        ELSE
           CALL TRANSCRIPT("")
	   CALL TRANSCRIPT("Bulk Thermodynamics Mode.") 
	   CALL TRANSCRIPT("")
        ENDIF
	RETURN
END SUBROUTINE PopBoundaryPart 