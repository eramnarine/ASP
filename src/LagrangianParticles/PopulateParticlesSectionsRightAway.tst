!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Add each of the individual modes to the Particles() array     !!
!! NOTE: Creates a single, internally mixed size distribution    !!
!! as the sum of the input modes.				 !!       
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE PopulateParticlesSectionsRightAway ()

	USE InfrastructuralCode, ONLY : INT2STR, REAL2STR, Transcript, &
                                        SetParticleID, ERROR

	USE ModelParameters,     ONLY : lengthscale,			&
					DomainX, DomainY, DomainZ,	&
					LagrSectSizeCutoff,		&
					ThermoBulkMode,			&
					HowManyBins,			&
					BinEdges,			&
					micron,				&
					SmallestAerosolPossible,        &
					Monodisperse,                   &
                                        Pi, AverageAerosolDensity

	USE GridPointFields,     ONLY : GetTemp, GetRelativeHumidity

	USE Chemistry,		ONLY :	HowManyAqChems,		        &
					AqMolecularMass,	        &
					howmanyaqanions,                &
                                        howmanyaqcations,               &
					HowManyOrgChems,	        &
					OrgMolecularMass,               &
                                        AqPhaseChemicalNames,           &
                                        HowManyAqOrgChems

	IMPLICIT NONE

	!! Local Variables
	INTEGER :: I, J, K, X, Y, Z, Q,	&
	           NumbParticles,	&
	           SectionalParticles,	&
		   LagrangianParticles

	REAL*8 :: II, Temperature, ln_sigma_sq, vtot, mtot, d_mass_mean
	REAL*8 :: del_diam, vrat, diam, mass_bin, store_num, d_num_mean
	REAL*8 :: sum_num, new_num, sum_mass, num_all_modes, rescale
	REAL*8 :: RescalingFactors(HowManyAerosolModes,HowManyAqChems+HowManyOrgChems)
        REAL*8 :: RH, InorgWaterContent

	CHARACTER (len= 1024) :: str
	LOGICAL :: Scaffolding, Transcribe, Jacobson
	TYPE(Particle), POINTER :: NewSection
	TYPE(Particle), POINTER :: Current, Trailer, SectionRunner

	Scaffolding= .FALSE.
	Transcribe = .TRUE.
        Jacobson = .TRUE. !Use Jacobson init or MELAM init

	IF(Scaffolding .OR. TRANSCRIBE) CALL Transcript("")
	IF(Scaffolding)	CALL Transcript(">>Entering PopulateParticlesSectionsRightAway()<<")
	IF(Transcribe)	CALL Transcript("_Assigning_Particles_to_Particle_Linked_List_Structure_")

	!! Start the aerosol ID numbering at 1
	CALL SetParticleID (1)

	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!! Each of the modes has its own sectional representation !!
	!! for particles below the certain size cutoff.			  !!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	IF (HowManyBins .GT. 0. .OR. ThermoBulkMode) THEN
		IF (.NOT.ThermoBulkMode .AND. .NOT.Monodisperse) THEN
			DO J = 1, HowManyBins
				NewSection => MakeSection(I,J)

				!!Budge the new mode in at the head of the list
				IF (ASSOCIATED(Particles%First)) THEN
					NewSection%Next	=> Particles%First
					Particles%First => NewSection
				ELSE
					Particles%First => NewSection
					NULLIFY(NewSection%Next)
					!NewSection%Next=> Null()
				END IF
			END DO

		ELSE IF (Monodisperse) THEN
			!Monodisperse Distribution
			!WRITE(*,*) "Check 0"
			HowManyBins = 1
			NewSection => MakeMonodisperse()
			!WRITE(*,*) "Check 0.5"
			Particles%First => NewSection
		
		ELSE
			!Bulk Mode
			NewSection => MakeBulk()
			Particles%First => NewSection
		END IF

	END IF

	IF(Scaffolding) WRITE(*,*) "Check 1", ThermoBulkMode 
	IF (.NOT.ThermoBulkMode .AND. .NOT.Monodisperse) THEN

!! Allocate all of the aerosol and place them in the appropriate structures:
                sum_num = 0.0
                sum_mass = 0.0
                num_all_modes = 0.0
	        DO I = 1, MAX(1,HowManyAerosolModes)
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		!! Scale the particle concentration by the number !!
		!! concentration and the domain volume.	          !!
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			NumbParticles = ANINT(AerosolModes(I,1) * DomainX/LengthScale * DomainY/LengthScale * DomainZ/LengthScale)
			SectionalParticles  = 0
			LagrangianParticles = 0
			IF(AerosolModes(I,3) .NE. 0.0) THEN !Log-normal mode

                                !MJA 05-29-2012 Old method, only integer numbers of particles
			    IF (NOT(Jacobson)) THEN
                                   DO J = 1, NumbParticles
					!Matt changed this to hopefully put particles in sections right away
					CALL MakeParticle(I, K, .FALSE., StructuredDistribution = (DFLOAT(J)-0.5)/NumbParticles)
					SectionalParticles  = SectionalParticles + 1 - K
					LagrangianParticles = LagrangianParticles + K
				  END DO
                            ELSE
                                !MJA 05-29-2012 New method
			        !The idea here is to populate the sections with mass and number based on Jacobson,
                                !Fundamentals of Atmospheric Modeling, 2005 
                                !Eq. 13.21 and 13.24 - MJA, 05-29-2012

                                !Calculate parameters out of loop if possible
                                ln_sigma_sq = LOG(AerosolModes(I,3))**2
                                d_num_mean = AerosolModes(I,2)*micron !Convert from um to cm 
                                !Jacobson, Eq. 13.25
                                vtot = (pi/6.0)*(d_num_mean**3.0)*exp(4.5*ln_sigma_sq)*AerosolModes(I,1)
                                mtot = vtot*AverageAerosolDensity

                                num_all_modes = num_all_modes +  AerosolModes(I,1)            
 
                                !Seinfeld and Pandis, 1998, Eq. 7.52
                                d_mass_mean = exp(log(d_num_mean)+3*ln_sigma_sq)

                                !Jacobson, Eq 13.6
                                vrat = (BinEdges(2,2)/BinEdges(2,1))**3.0


                                !Loop over sections
                                Current => Particles%First                        
 99			        IF (Current%Sectional .EQ. .TRUE. .AND. Current%Edges(2) .LT. 1.0E4 ) THEN
                           
                                    !Jacobson, Eq 13.9
                                    del_diam = 2.0*(Current%Edges(2)-Current%Edges(1)) 

                                    !Jacobson, Eq. 13.9
                                    diam = del_diam/(2.0**(1.0/3.0))
                                    diam = diam*(1.0+vrat)**(1.0/3.0)
                                    diam = diam/(vrat**(1.0/3.0)-1.0)
                                    !Write(*,*) vrat, del_diam, diam, 2*Current%Edges(2), 2*Current%Edges(1), d_num_mean
                                    !STOP
                                    !Jacobson, Eq. 13.24: Number of particles
                                    store_num = Current%NumberOfParticles

                                    if (Current%Edges(1) .NE. 0.0) then !Skip first bin for now
                                         new_num = (AerosolModes(I,1)*(del_diam/diam) &
                                                       *exp(-0.5*(log(diam/d_num_mean)**2)/ln_sigma_sq) &
                                                       /sqrt(2*Pi*ln_sigma_sq))
                                         if(new_num .gt. 1.0e-6) then !Skip bins with less than 1e-6 particles/cm3
                                           sum_num = sum_num + new_num
                                           Current%NumberOfParticles = Current%NumberOfParticles + new_num
                                         endif
                                    else
                                         Current%NumberOfParticles = 0.0 !Skip first bin for now
                                    endif

                                    !Jacobson, Eq. 13.21: Mass of particles
                                    if (Current%Edges(1) .NE. 0.0) then !Skip first bin for now
                                       mass_bin = mtot*(del_diam/diam) &
                                                      *exp(-0.5*(log(diam/d_mass_mean)**2)/ln_sigma_sq) &
                                                      /sqrt(2*Pi*ln_sigma_sq)
                                       write(*,*) "mass check: ", mass_bin, d_mass_mean
                                       if(new_num .gt. 1.0e-6) then !Skip bins with less than 1e-6 particles/cm3
                                          sum_mass = sum_mass+mass_bin
                                       endif
                                    else
                                       sum_mass = 0.0
                                    endif

                                    if(Current%NumberOfParticles .gt. 1.0e-6 .and. Current%Edges(1) .NE. 0.0) then 
                                     !Skip bins with less than 1e-6 particles/cm3
                                     !and first bin
	                              DO J = 1, HowManyAqChems !MJA, 02-28-2012
                                        !WRITE(*,*) "MakeParticle: ", AverageAerosolDensity
                                         Current%AqChems(J) = (Current%AqChems(J)*store_num &
                                                              + AerosolModes(I,3+J) *mass_bin/AqMolecularMass(J))		&
							     /Current%NumberOfParticles
			              END DO
			
			              DO J = 1, HowManyOrgChems
				         Current%OrgChems(J) = (Current%OrgChems(J)*store_num &
                                                              + AerosolModes(I,3+HowManyAqChems+J) *mass_bin/OrgMolecularMass(J)) &
							     /Current%NumberOfParticles
			              END DO
                                    else
                                      DO J = 1, HowManyAqChems
                                        !WRITE(*,*) "MakeParticle: ", AverageAerosolDensity
                                         Current%AqChems(J) = 0.0
			              END DO
			
			              DO J = 1, HowManyOrgChems
				         Current%OrgChems(J) = 0.0
			              END DO
                                    endif

			            !NOTE: We initialize with all of the organic compounds in 
			            !the organic phase, and none in the hydrophilic phase
			            DO J = 1, HowManyAqOrgChems
				         Current%AqOrgChems(J) = 0.0
		 	            END DO
                                ENDIF

                                IF(ASSOCIATED(Current%Next)) THEN
				     Current => Current%Next
				     GOTO 99
                                ENDIF
                        
                            ENDIF
		            !! Report the results and allocations of the population
			    IF (TRANSCRIBE) CALL TRANSCRIPT ("")
			    IF (TRANSCRIBE) CALL TRANSCRIPT ("For the '"//TRIM(AerosolModeNames(I))//"' aerosol mode")
			    IF (TRANSCRIBE) CALL TRANSCRIPT ("Allocated "//TRIM(INT2STR(LagrangianParticles))//" Lagrangian particles")
			    IF (TRANSCRIBE) CALL TRANSCRIPT ("and "//TRIM(INT2STR(SectionalParticles))//" Sectional particles")
			ELSE !Exponential Mode
				CALL MakeParticleExponential(I)
			END IF
		END DO
	

	END IF


	IF(Scaffolding) WRITE(*,*) "Check 1.5"

	!! If the user specified that the contain a certain mass of each 
	!! species then rescale everything
	IF (InputFlagRatioOrMass .EQ. 1) THEN
		
		!!This should only be called for monodisperse or bulk aerosol
		IF(NOT(Monodisperse) .AND. NOT(ThermoBulkMode)) &
			CALL ERROR ("You selected a sectional aerosol.  In this mode, you must specify the abundance of chemicals by "// &
						"the relative mass percentage using the second flag in AerosolModes.in (0); you have selected another "// &
						"option.  Please remedy.")	

		DO I = 1, HowManyAerosolModes
		DO J = 1, HowManyAqChems+HowManyOrgChems
			RescalingFactors(I,J) = 0.
		END DO ; END DO

		Current => PARTICLES%First

	10	IF (ASSOCIATED(Current)) THEN

			DO I = 2,HowManyAqChems
				RescalingFactors(Current%ParticleDistribution,I) = RescalingFactors(Current%ParticleDistribution,I) + &
				Current%AqChems(I)*Current%NumberOfParticles
			END DO

			DO I = 1,HowManyOrgChems
				RescalingFactors(Current%ParticleDistribution,HowManyAqChems+I) = RescalingFactors(Current%ParticleDistribution,HowManyAqChems+I) + &
				Current%OrgChems(I)*Current%NumberOfParticles
			END DO

			IF (ASSOCIATED(Current%Next)) THEN
				Current => Current%Next
				GOTO 10
			END IF
		END IF 

		IF(Scaffolding) WRITE(*,*) "Check 2"


		!! Then Calculate the Scaling
		DO I = 1, MAX(1,HowManyAerosolModes-1)
			DO J = 2, HowManyAqChems
				IF (RescalingFactors(I,J) .GT. 0.) &
					RescalingFactors(I,J) = AerosolModes(I,J+3) /		&
											RescalingFactors(I,J) / 1.e12 / AqMolecularMass(J)
			END DO 
	
			DO J = 1, HowManyOrgChems
				IF (RescalingFactors(I,HowManyAqChems+J) .GT. 0.) &
					RescalingFactors(I,HowManyAqChems+J) = AerosolModes(I,HowManyAqChems+J+3) /		&
											RescalingFactors(I,HowManyAqChems+J) / 1.e12 / OrgMolecularMass(J)
			END DO 
		
		
		END DO

		IF(Scaffolding) WRITE(*,*) "Check 3"
		Current => PARTICLES%First

	20	IF (ASSOCIATED(Current)) THEN

			DO I = 2,HowManyAqChems
				Current%AqChems(I) = Current%AqChems(I) * RescalingFactors(Current%ParticleDistribution,I)
			END DO

			DO I = 1,HowManyOrgChems
				Current%OrgChems(I) = Current%OrgChems(I) * RescalingFactors(Current%ParticleDistribution,HowManyAqChems+I)
			END DO
				
			IF (ASSOCIATED(Current%Next)) THEN
				Current => Current%Next
				GOTO 20
			END IF
		END IF
	END IF


	IF(Scaffolding) WRITE(*,*) "Check 4"


	!! Now that all of the consitutents have been added,
	!! walk through the lists and equilibrate everything
	Current => PARTICLES%First
	NULLIFY(Trailer)
	!Trailer => Null()

30	IF (ASSOCIATED(Current)) THEN

		IF (Current%NumberOfParticles .GT. 0.0) THEN
			II = 0.
			DO I = 2, HowManyAqChems
				!IF (Current%AqChems(I) .NE. 0.0) WRITE(*,*) AqPhaseChemicalNames(I)
				II = II + Current%AqChems(I)
			END DO
			DO I = 2, HowManyOrgChems
				!IF (Current%AqChems(I) .NE. 0.0) WRITE(*,*) AqPhaseChemicalNames(I)
				II = II + Current%OrgChems(I)
			END DO

                        WRITE(*,*) "H2O: ", Current%AqChems(1)
			!! Add a token amount of water to avoid initial numerical problems
			IF (II .GT. 0.0) THEN
                           Current%AqChems(1) = II*1.
                        ELSE
                           !If only BC specified, particle is dry
                           Current%Dry = .TRUE.
                        ENDIF
          


			IF (NOT(Current%Dry)) THEN 
                           CALL KusikMeissner (Current)
			   IF(Scaffolding) WRITE(*,*) "Before Equilibrate"

                           CALL FindElectrolyteEquilibrium(Current, .TRUE., FirstEquilibration=.TRUE., ReturnType = Q)
			   IF(Scaffolding) WRITE(*,*) "After Equilibrate"

		           RH =  GetRelativeHumidity ()
                           CALL EquilibriumWaterContentAmount (Current, RH, Q, .TRUE., InorgWaterContent)
                           Current%AqChems(1) = InorgWaterContent
			   IF(Scaffolding) WRITE(*,*) "After Water EQ"
			ENDIF
                         WRITE(*,*) "H2O Second: ", Current%AqChems(1)
                        CALL RecalculateRadius (Current)
				IF(Scaffolding) WRITE(*,*) "After RecalculateRadius"

                        !CALL ShellRefIndAndRad(Current)
				IF(Scaffolding) WRITE(*,*) "After ShellRefIndAndRad"
		
		END IF

		Trailer => Current
		Current => Current%Next		
                GOTO 30
	END IF

	!! Report what ended up with
	IF(NOT(Monodisperse) .AND. NOT(ThermoBulkMode)) THEN
           CALL TRANSCRIPT("")
	   CALL TRANSCRIPT("Reallocated "//TRIM(INT2STR(SectionalParticles))//" particles to sections and left "//		&
	       TRIM(INT2STR(LagrangianParticles))//" as lagrangian particles.")
	   CALL TRANSCRIPT("")
        ELSEIF(Monodisperse) THEN
           CALL TRANSCRIPT("")
	   CALL TRANSCRIPT("Monodisperse distribution.") 
	   CALL TRANSCRIPT("")
        ELSE
           CALL TRANSCRIPT("")
	   CALL TRANSCRIPT("Bulk Thermodynamics Mode.") 
	   CALL TRANSCRIPT("")
        ENDIF
	RETURN
END SUBROUTINE PopulateParticlesSectionsRightAway